// config.js
module.exports = {
    NODE_ENV: process.env.NODE_ENV || 'development',
    API: process.env.API || 'https://swapi.dev/api',
    PORT: process.env.PORT || 3000
}