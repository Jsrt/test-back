const express = require('express')
const cors = require('cors')

const config = require('./config/config')

const app = express()

const People = require('./models/people')
const Planet = require('./models/planet')

const people = new People()
const planet = new Planet();

app.use(cors())

app.get('/people', (req, res) => {

    let { sort, page } = req.query

    people.getPeople(page, sort)
        .then(resp => {
            res.send(resp)
        })

})

app.get('/people/:id', (req, res) => {

    let { id } = req.params

    people.getPeopleForId(id)
        .then(resp => res.send(resp))
})

app.get('/planets', (req, res) => {

    let { page } = req.query
     
    planet.getPlanet(page)
        .then(resp => res.send(resp))
})

app.get('*', (req, res) => {
    res.send('404 | Page not found')
})


app.listen(config.PORT)