const axios = require("axios");
const config = require("../config/config");


class People {
    constructor () {
        //TOD0:
    }


    async getPeople( page = 1, sort = undefined ) {
        const options = ['nombre', 'peso', 'altura'];
        
        try {
            if (sort !== undefined) {
                
                let response = await axios.get(`${config.API}/people/?page=${page}`)

                let type = options[options.indexOf(sort)]
                
                response.data.results.sort((a, b) => {
                    switch (type) {
                        case 'nombre':
                            if (a.name > b.name) {
                                return 1;
                            }
            
                            if (a.name < b.name) {
                                return -1;
                            }
                            break;
                        case 'peso':
                            if (Number(a.mass) > Number(b.mass)) {
                                return 1;
                            }
            
                            if (Number(a.mass) < Number(b.mass)) {
                                return -1;
                            }
                            break;
                        case 'altura':
                            if (Number(a.height) > Number(b.height)) {
                                return 1;
                            }
            
                            if (Number(a.height) < Number(b.height)) {
                                return -1;
                            }

                            break;
                    }
    
                      // a must be equal to b
                      return 0;
                })

                return response.data

            } else {
                let response = await axios.get(`${config.API}/people/?page=${page}`)
                return response.data
            }
        } catch (error) {
            console.error(error)
        }


        
    }

    async getPeopleForId (id = 0) {
        try {
            const response = await axios.get(`${config.API}/people/${id}`)
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

}


module.exports = People