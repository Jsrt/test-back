const axios = require("axios");
const config = require("../config/config");


class Planet {

    planets = []
    residents = []

    constructor() {

    }

    async getPlanet(page = 1) {
        
        try {
            const response = await axios.get(`${config.API}/planets/?page=${page}`)

            let data = response.data;
            let newData;

            newData = data.results.map(el => {
                return {
                    planet: el.name,
                    residents: el.residents
                }
            })

            return newData
        } catch (error) {
            console.error(error)
        }
    }

}

module.exports = Planet